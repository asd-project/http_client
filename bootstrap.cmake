
execute_process(COMMAND asd.bat root OUTPUT_VARIABLE ROOT_PATH ERROR_VARIABLE ROOT_PATH RESULT_VARIABLE RESULT WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})

if(NOT "${RESULT}" EQUAL 0)
    message(FATAL_ERROR "Couldn't locate root path: ${ROOT_PATH}")
endif()

include("${ROOT_PATH}/project_tools/init_package.cmake")
include("${ROOT_PATH}/project_tools/locate_package.cmake")

locate_package(asd.build_tools BUILD_TOOLS_FOLDER)

include("${BUILD_TOOLS_FOLDER}/workspace.cmake")
include(module)
