import os

from conans import ConanFile, CMake

project_name = "http_client"


class HttpClientConan(ConanFile):
    name = "asd.%s" % project_name
    version = "0.0.1"
    license = "MIT"
    author = "bright-composite"
    url = "https://gitlab.com/asd-project/%s" % project_name
    description = "Simple http client components"
    topics = ("asd", project_name)
    exports_sources = "include*", "CMakeLists.txt", "bootstrap.cmake"
    requires = (
        "asd.build_tools/0.0.1@bright-composite/testing",
        "zlib/1.2.11",
        "openssl/1.1.1f",
        "boost_beast/1.69.0@bincrafters/stable",
        "boost_certify/0.1.0@bright-composite/testing",
        "jsonformoderncpp/3.7.3@vthiery/stable",
    )

    def source(self):
        pass

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        self.copy("*.h", dst="include", src="include")
        self.copy("*.hpp", dst="include", src="include")
        self.copy("*.ipp", dst="include", src="include")