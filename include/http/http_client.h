//---------------------------------------------------------------------------

#pragma once

#ifndef HTTP_HTTP_CLIENT_H
#define HTTP_HTTP_CLIENT_H

//---------------------------------------------------------------------------

#include <boost/beast.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ssl/error.hpp>
#include <boost/asio/ssl/stream.hpp>

#include <boost/certify/extensions.hpp>
#include <boost/certify/https_verification.hpp>

#include <nlohmann/json.hpp>

//---------------------------------------------------------------------------

namespace asd
{
    namespace http
    {
        using namespace std::chrono_literals;
        using namespace boost::beast::http;

        namespace beast = boost::beast;
        namespace ssl = boost::asio::ssl;

        using tcp = boost::asio::ip::tcp;
        using json = nlohmann::json;

        using headers_type = std::map<std::string, std::string>;
        using query_type = std::multimap<std::string, std::string>;
        using payload_type = std::multimap<std::string, std::string>;
        using ssl_stream = ssl::stream<tcp::socket>;

        struct connection_options
        {
            int retry_attempts = 0;
            std::chrono::milliseconds retry_delay = 500ms;
        };

        std::string url_encode(const std::string & value);

        std::string query_string(const query_type & query);
        std::string payload_string(const payload_type & payload);

        class connection
        {
        public:
            virtual std::string get(const std::string & path, const headers_type & headers, const std::string & query = {}, const std::string & accepted_type = "application/json") = 0;
            virtual std::string post(const std::string & path, const headers_type & headers, const std::string & payload = {}, const std::string & content_type = "text/plain") = 0;

            std::string get(const std::string & path, const headers_type & headers, const query_type & query, const std::string & accepted_type = "application/json") {
                return get(path, headers, query_string(query), accepted_type);
            }

            std::string post(const std::string & path, const headers_type & headers, const payload_type & payload) {
                return post(path, headers, payload_string(payload), "application/x-www-form-urlencoded");
            }

            std::string post(const std::string & path, const headers_type & headers, const json & payload) {
                return post(path, headers, payload.dump(), "application/json");
            }
        };

        template <class F>
        void connect(const std::string & host, boost::asio::io_context & context, const connection_options & options, F callback);
    }
}

#include "http_client.ipp"

//---------------------------------------------------------------------------
#endif
