//---------------------------------------------------------------------------

#include <http/http_client.h>

#include <sstream>
#include <iomanip>

//---------------------------------------------------------------------------

namespace asd
{
    namespace http
    {
        template <class Stream>
        std::string make_request(Stream & stream, const http::request<http::string_body> & request) {
            http::write(stream, request);

            boost::beast::flat_buffer buffer;
            http::response<http::string_body> res;

            http::read(stream, buffer, res);

            auto status_class = http::to_status_class(res.result());

            if (status_class != http::status_class::successful) {
                throw std::runtime_error(res.reason().to_string() + " (" + res.body() + ")");
            }

            return res.body();
        }

        template <class Stream>
        class connection_impl : public connection
        {
        public:
            connection_impl(const std::string & host, Stream & stream) : host(host), stream(stream) {}

            std::string get(const std::string & path, const headers_type & headers, const std::string & query, const std::string & accepted_type) override {
                std::string full_path = path + query;

                http::request<http::string_body> request{http::verb::get, full_path, 11};
                request.set(http::field::host, host);
                request.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING);
                request.set(http::field::accept, accepted_type);

                for (auto & entry : headers) {
                    request.set(entry.first, entry.second);
                }

                return make_request(stream, request);
            }

            std::string post(const std::string & path, const headers_type & headers, const std::string & payload, const std::string & content_type) override {
                std::string full_path = path;

                http::request<http::string_body> request{http::verb::post, full_path, 11};
                request.set(http::field::host, host);
                request.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING);
                request.set(http::field::content_type, content_type);
                request.body() = payload;
                request.prepare_payload();

                for (auto & entry : headers) {
                    request.set(entry.first, entry.second);
                }

                return make_request(stream, request);
            }

        private:
            const std::string & host;
            Stream & stream;
        };

        inline std::string url_encode(const std::string & value) {
            std::ostringstream escaped;
            escaped.fill('0');
            escaped << std::hex;

            for (auto & c : value) {
                auto b = static_cast<uint8_t>(c);

                if (std::isalnum(b) || b == '-' || b == '_' || b == '.' || b == '~') {
                    escaped << b;
                    continue;
                }

                escaped << std::uppercase;
                escaped << '%' << std::setw(2) << static_cast<int>(c);
                escaped << std::nouppercase;
            }

            return escaped.str();
        }

        inline std::string query_string(const query_type & query) {
            if (query.empty()) {
                return "";
            }

            std::string s = "?";

            for (auto it = query.begin(); it != query.end(); ++it) {
                if (it != query.begin()) {
                    s += "&";
                }

                s += url_encode(it->first);

                if (!it->second.empty()) {
                    s += "=" + url_encode(it->second);
                }
            }

            return s;
        }

        inline std::string payload_string(const payload_type & payload) {
            if (payload.empty()) {
                return "";
            }

            std::string s;

            for (auto it = payload.begin(); it != payload.end(); ++it) {
                if (it != payload.begin()) {
                    s += "&";
                }

                s += url_encode(it->first);

                if (!it->second.empty()) {
                    s += "=" + url_encode(it->second);
                }
            }

            return s;
        }

        enum class protocol {
            unknown,
            http,
            https
        };

        struct host_info
        {
            host_info() = default;
            host_info(host_info &&) = default;

            host_info & operator = (host_info &&) = default;

            http::protocol protocol = http::protocol::unknown;
            std::string host_name;
            int port = 443;
        };

        inline host_info parse_host(const std::string & s) {
            host_info info;

            info.host_name = s;
            auto slash_pos = info.host_name.find("://");

            if (slash_pos != std::string::npos) {
                auto protocol_string = info.host_name.substr(0, slash_pos);

                if (protocol_string == "http") {
                    info.protocol = http::protocol::http;
                } else if (protocol_string == "https") {
                    info.protocol = http::protocol::https;
                }

                info.host_name = info.host_name.substr(slash_pos + 3);
            }

            auto target_separator = info.host_name.find("/");

            if (target_separator != std::string::npos) {
                throw std::runtime_error("Host string should not contain subpath: " + s);
            }

            auto port_separator = info.host_name.find(":");

            if (port_separator != std::string::npos) {
                try {
                    info.port = std::stoi(info.host_name.substr(port_separator + 1));
                } catch (const std::invalid_argument &) {
                    throw std::runtime_error("Host port is not correct integer");
                }

                info.host_name = info.host_name.substr(0, port_separator);
            }

            if (info.protocol == http::protocol::unknown) {
                if (info.port == 80) {
                    info.protocol = http::protocol::http;
                } else if (info.port == 443) {
                    info.protocol = http::protocol::https;
                }
            }

            return info;
        }

        template <class F>
        void connect(const std::string & host, boost::asio::io_context & io_context, const connection_options & options, F callback) {
            tcp::resolver resolver{io_context};
            tcp::socket socket{io_context};

            http::host_info host_info = parse_host(host);

            switch (host_info.protocol) {
                case http::protocol::http: {
                    int attempts = 0;

                    while (true) {
                        ++attempts;

                        try {
                            const auto results = resolver.resolve(host_info.host_name, std::to_string(host_info.port));
                            boost::asio::connect(socket, results.begin(), results.end());
                            break;
                        } catch (const boost::system::system_error & e) {
                            if (attempts > options.retry_attempts) {
                                throw e;
                            }

                            std::this_thread::sleep_for(options.retry_delay);
                        }
                    }

                    http::connection_impl<tcp::socket> connection {host_info.host_name, socket};

                    callback(connection);

                    boost::system::error_code ec;
                    socket.close(ec);

                    if(ec && ec != boost::asio::error::eof) {
                        throw boost::system::system_error{ec};
                    }

                    break;
                }

                case http::protocol::https: {
                    ssl::context ssl_ctx(ssl::context::tlsv12_client);

                    ssl_ctx.set_verify_mode(ssl::context::verify_peer | ssl::context::verify_fail_if_no_peer_cert);
                    ssl_ctx.set_default_verify_paths();

                    boost::certify::enable_native_https_server_verification(ssl_ctx);

                    int attempts = 0;

                    while (true) {
                        ++attempts;

                        try {
                            const auto results = resolver.resolve(host_info.host_name, std::to_string(host_info.port));
                            boost::asio::connect(socket, results.begin(), results.end());
                            break;
                        } catch (const boost::system::system_error & e) {
                            if (attempts > options.retry_attempts) {
                                throw e;
                            }

                            std::this_thread::sleep_for(500ms);
                        }
                    }

                    http::ssl_stream ssl_stream(std::move(socket), ssl_ctx);

                    boost::certify::set_server_hostname(ssl_stream, host_info.host_name);
                    boost::certify::sni_hostname(ssl_stream, host_info.host_name);

                    ssl_stream.handshake(ssl::stream_base::client);

                    http::connection_impl<http::ssl_stream> connection {host_info.host_name, ssl_stream};

                    callback(connection);

                    boost::system::error_code ec;
                    ssl_stream.shutdown(ec);
                    ssl_stream.next_layer().close(ec);

                    if(ec && ec != boost::asio::error::eof) {
                        throw boost::system::system_error{ec};
                    }

                    break;
                }

                default: {
                    throw std::runtime_error("Unknown connection protocol: " + host);
                }
            }
        }
    }
}

//---------------------------------------------------------------------------
